xdvik-ja (22.87.06+j1.42-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Janitor ]
  * Fix some issues reported by lintian

  [ Youhei SASAKI ]
  * Update d/changelog
  * d/control: B-d, use pkgconf instead of pkg-config

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 29 Mar 2024 18:08:50 +0900

xdvik-ja (22.87.06+j1.42-2) unstable; urgency=medium

  * Add patch: support 2004-{H,V} entry (Closes: #1052607)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 13 Oct 2023 17:41:32 +0900

xdvik-ja (22.87.06+j1.42-1) unstable; urgency=medium

  * Bump Standards-Version to 4.6.2 (no changes needed)
  * New upstream version 22.87.06+j1.42
  * Change location config.xdvi
  * Update README.Debian (Closes: #993597)
  * debian/control: Remove empty control field Recommends in package xdvik-ja.
  * d/control: Add build-depends libfreetype1-dev (Closes: #1003328)
  * d/control: Update Build-Depends
  * Typo. fixed
  * Refresh patches
  * d/patches: Add DEP-3 tag
  * Fix use-after-free bug (Closes: #1003327) Thanks to NIDE-san!
  * Add source/lintian-overrides: source-is-missing

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 05 Oct 2023 15:44:56 +0900

xdvik-ja (22.87.05+j1.42-3) unstable; urgency=medium

  * Update debian/salsa-ci.yml file
  * Bump debhelper from old 12 to 13.
  * Fix day-of-week for changelog entry 22.40u-j1.12-0.2.
  * Update standards version to 4.5.1, no changes needed.
  * d/rules: update for dh compat = 13
  * d/control: Add texlive-lang-japanese and poppler-data into Depends
    (Closes: #993597)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sat, 04 Sep 2021 22:08:27 +0900

xdvik-ja (22.87.05+j1.42-2) unstable; urgency=medium

  * Re-Add debian/salsa-ci.yml
  * Imported Upstream version 22.87.03+j1.42
  * New upstream version 22.87.05
  * Refresh patches
  * Add d/control: Add Rules-Requires-Root
  * Update debian/changelog
  * d:control: Update Vcs-* field

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 19 Apr 2020 13:08:15 +0900

xdvik-ja (22.87.05+j1.42-1) unstable; urgency=medium

  * Drop salsa-ci.yml
  * New upstream version 22.87.05
  * Refresh patches
  * Add d/control: Add Rules-Requires-Root

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 30 Mar 2020 15:04:42 +0900

xdvik-ja (22.87.03+j1.42-4) unstable; urgency=medium

  * Add pkg-config into Build-Depends (Closes: #952100)
  * d/{control,compat}: use debhelper-compat
  * d/copyright: update copyright year
  * Add salsa-ci.yml
  * d/control: Bump Standard Version 4.5.0

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 25 Feb 2020 10:18:25 +0900

xdvik-ja (22.87.03+j1.42-3) unstable; urgency=medium

  * Add patch: follow gs9.27 changes (Closes: #939494)
    Thanks to NIDE, Naoyuki!
  * d/control: Bump Standard Version 4.4.0
  * d/{control,compat}: Bump Compat 12

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 09 Sep 2019 15:20:37 +0900

xdvik-ja (22.87.03+j1.42-2) unstable; urgency=medium

  [ Youhei SASAKI ]
  * d/control: Bump Standard-Version 4.2.1
  * d/control: update Vcs-{Git,Browser}. use salsa
  * d/changelog: remove trailing-whitespace

  [ Hilko Bengen ]
  * Add patch to detect freetype2 via pkg-config (Closes: #892353)
  * Debian release

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 06 Nov 2018 15:06:53 +0900

xdvik-ja (22.87.03+j1.42-1) unstable; urgency=medium

  * Add debian/watch: use SourceForge as upstream
    - Bump version 4
  * Imported Upstream version 22.87.03+j1.42
  * Refresh, split patches for copyright
  * Drop obsolete lintian overrides
  * Update debian/rules: follow upstream change
  * Drop autotool generated file: c-auto.in
  * Update debian/copyright: format 1.0
    - Add Files-Excluded field in order to strip TeXLive build files
  * Update debian/control:
    - Drop obsolete: DM-Upload-Allowed line
    - Update Maintainer, drop Uploaders
    - Update Vcs-* field
  * Bump Compat: 9
  * Bump Standard version: 3.9.8
  * Add README.Debian
  * Update debhelper script: cosmetic, drop defoma entry

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 06 Nov 2018 15:06:51 +0900

xdvik-ja (22.84.16-j1.40+t1lib-1) unstable; urgency=low

  [Youhei SASAKI]
  * New upstream release: TeXLive 2011(2012/dev) with t1lib + e-pTeX patches
  * Build with internal t1lib provided by TeXLive upstream sources
    - As t1lib is going to dissappear in Wheezy (Closes: #638764).
      There are no alternatives for CJKV DVI, and replacement to freetype 2 is
      still in progress.
    - I know, we should be avoided. libfreetype2 should be use instead.

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 16 Apr 2012 14:46:31 +0900

xdvik-ja (22.84.16-j1.40-1) unstable; urgency=low

  [Youhei SASAKI]
  * New upstream release: TeXLive 2011(2012/dev) + apply e-pTeX patches
  * Bump Standard-Version: 3.9.3
  * Change debian/rules: use dh instead of CDBS
  * Update, rename patches:
    - 0001-tl11supp-pxdvi-120120.patch: e-pTeX patch, modified for Debian
    - 0002-Fix-Werror-format-security.patch
    - 0003-Fix-manpage-error.patch
    - 0004-Fix-Freetype-Invalid-Outline.patch
  * Drop obsolete patches:
    - 51_ld_as-needed.diff: use dh_autoreconf --as-needed
    - 10gcc.diff, 30common.mk, 40Makefile.in.diff, 50libtool-tag.diff:
      These patches obsolete because of upstream changes.

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 15 Mar 2012 04:42:29 +0900

xdvik-ja (22.84.13-j1.34-4) unstable; urgency=low

  [Youhei SASAKI]
  * Add DM-Upload-Allowed: yes
  * Bump Standard-Version: 3.9.2
  * Update Depends: Font name changed.
    Thanks to Hideki Yamane (Closes: #642121, #642210)
  * Fix FTBFS with ld --as--needed.
    Thanks to Matthias Klose (Closes: #641292, LP: #832899)
  * Fix FTBFS: dpkg-buildflags
    Thanks to Hideki Yamane, Daniel T Chen (Closes: #644048)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 21 Sep 2011 14:23:32 +0900

xdvik-ja (22.84.13-j1.34-3) unstable; urgency=low

  [Youhei SASAKI]
  * Add me to Uploaders, thanks to Tsuchiya-san, Kohda-san.
  * Bump Standard-version: 3.8.4
  * Change source format: 3.0 (quilt)
  * Fix invalid outline from freetype (Closes: #583874).
  * Fix some lintian warnings:
    - Add copyright notice about original xdvik, localization patches
    - fix manpages error
  * Fix description in update-vfontmap: remove defoma entry.

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 11 Jun 2010 13:38:10 +0900

xdvik-ja (22.84.13-j1.34-2.2) unstable; urgency=high

  * NMU.  Fix debian/xdvik-ja.prerm to close serious bug, so urgency is high.
    Thanks to Yamane-san <henrich AT debian.or.jp> for his help.
    (Closes: #574194)
  * debian/xdvik-ja.preinst
    - call defoma-app to remove unnecessary defoma entry.

 -- Atsuhito KOHDA <kohda@debian.org>  Fri, 26 Mar 2010 16:16:09 +0900

xdvik-ja (22.84.13-j1.34-2.1) unstable; urgency=low

  * NMU.  In fact this is a co-operation with a maintainer and an uploader.
  [TSUCHIYA Masatoshi]
  * Removed defoma from depends and build-dep - closes: #458863
  * Call libtool with `--tag=cc' option - closes: #511908
  [Atsuhito Kohda]
  * Fixed to work under TeXLive2009 (kpathsea5).  (Closes: #527526, #560964)
  * Changed sponsor and uploader.
  * Fixed control, compat, rules and prerm files to erase lintian warnings.
   - added ${misc:Depends} and ttf-sazanami-mincho, ttf-vlgothic in control.
   - changed from 4 to 7 in compat file.
   - so replaced "dh_clean -k" with "dh_prep" in rules file.
   - removed prepended path in prerm.

 -- Atsuhito KOHDA <kohda@debian.org>  Tue, 05 Jan 2010 12:58:43 +0900

xdvik-ja (22.84.13-j1.34-2) unstable; urgency=low

  * Removed libwww-dev from build-dep - closes: #458863

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Tue, 08 Jan 2008 02:38:01 +0900

xdvik-ja (22.84.13-j1.34-1) unstable; urgency=low

  [TSUCHIYA Masatoshi]
  * New upstream release.

  [Masayuki Hatta]
  * Bumped up Standards-Version to 3.7.3 (no physical changes).

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Sun, 06 Jan 2008 15:42:48 +0900

xdvik-ja (22.84.12-j1.34-1) unstable; urgency=low

  * New upstream release. - closes: #336783
  * Remove libkpathsea4-dev from Build-Depends. - closes: #429679
  * Workaround patches (debian/patches/60kpse_enc_file.diff,
    debian/patches/80wideprototype.diff) are removed.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Sat, 28 Jul 2007 18:47:57 +0900

xdvik-ja (22.84.10-j1.33-1) unstable; urgency=low

  * New upstream release.
  * Configuration file paths are changed, because format of vfontmap is
    changed by upstream developers.
  * Use defoma to select appropriate fonts.
  * Added ttf-japanese-* to Depends.
  * Added texlive-base-bin to Depends - closes: #357200

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Wed, 27 Sep 2006 18:48:59 +0900

xdvik-ja (22.84.8-j1.22-2) unstable; urgency=low

  * Works done at Codefest in Malaysia 2006.
  * Added me to Uploaders.
  * Tighten up dependencies.
  * Added ttf-sazanami-* to Depends - closes: #352006
  * Bumped to Standards-Version: 3.6.2.2 (no physical changes).

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Sat,  4 Mar 2006 12:48:52 +0900

xdvik-ja (22.84.8-j1.22-1) unstable; urgency=low

  * New upstream release (closes: #277335)

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Tue, 05 Apr 2005 17:59:29 +0900

xdvik-ja (22.84.8-j1.21-1) unstable; urgency=low

  * New upstream release

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Sat, 01 Jan 2005 17:17:56 +0900

xdvik-ja (22.84.5-j1.21-1) unstable; urgency=low

  * New upstream release

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Sat, 06 Nov 2004 23:40:04 +0900

xdvik-ja (22.84.3-j1.21-2) unstable; urgency=low

  * debian/patches/80tempfile-fix.diff: Change to avoid the reported bug.
    Thanks to Kenshi Muto and Fumitoshi UKAI for their kind help.
    - closes: #272437

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Wed, 29 Sep 2004 15:00:23 +0900

xdvik-ja (22.84.3-j1.21-1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Wed, 25 Aug 2004 11:46:43 +0900

xdvik-ja (22.84.1-j1.21-1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Mon, 28 Jun 2004 12:16:13 +0900

xdvik-ja (22.84-j1.21-2) unstable; urgency=low

  * Add tmin and tgoth to 20ascii-ptex.map.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Wed, 16 Jun 2004 23:28:16 +0900

xdvik-ja (22.84-j1.21-1) experimental; urgency=low

  * New upstream release.
  * Use experimental Japanized patch.
  * debian/10keybind-option.diff: Removed.
  * Use libt1-dev instead of t1lib-dev. - closes: #251185

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Mon, 14 Jun 2004 01:01:42 +0900

xdvik-ja (22.40y-j1.18-1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Tue, 30 Sep 2003 04:07:52 +0900

xdvik-ja (22.40y-j1.17-1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Thu, 21 Aug 2003 00:19:27 +0900

xdvik-ja (22.40x-j1.17-1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Fri, 25 Apr 2003 18:17:38 +0900

xdvik-ja (22.40w-j1.17-2) unstable; urgency=low

  * Add libwww0 to Build-Depends filed of debian/control.  This addition
    must be done in 22.40v-j1.14-3, but, I made a mistake. - closes: #189117

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Tue, 15 Apr 2003 18:36:41 +0900

xdvik-ja (22.40w-j1.17-1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Thu, 10 Apr 2003 11:02:32 +0900

xdvik-ja (22.40v-j1.14-3) unstable; urgency=low

  * Updated Standard-Version: 3.5.9
  * Now Build-Depends: libwww-dev and libwww0 - closes: #188108

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Sat, 22 Mar 2003 10:13:19 +0900

xdvik-ja (22.40v-j1.14-2) unstable; urgency=low

  * debian/patches/20scalebox.diff: A patch to fix scalebox bug.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Sat, 22 Mar 2003 10:13:19 +0900

xdvik-ja (22.40v-j1.14-1) unstable; urgency=low

  * New upstream release.
  * debian/xdvi-pl: Fix typo reported by Atsuhito Kohda <kohda@pm.tokushima-u.ac.jp>.
  * debian/postinst: Fix a misusage of dpkg-divert.
  * Now upstream supports source special feature - closes: #94244

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Sun, 16 Mar 2003 21:37:57 +0900

xdvik-ja (22.40v-j1.13-1) unstable; urgency=low

  * New Maintainer.
  * Sponsored by Masayuki Hatta <mhatta@debian.org>.
  * New upstream release - closes: #76355
  * Now Build-Depends: libxaw7-dev - closes: #170011
  * Now /usr/bin/xdvi-ja has gone - closes: #52853
  * Now generates /etc/texmf/vfontmap - closes: #138885

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Sat,  8 Mar 2003 02:59:14 +0900

xdvik-ja (22.40v-j1.13-0.2) unstable; urgency=low

  * debian/01ja-fix.diff: Fix bugs on upstream patch.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Tue, 11 Feb 2003 13:31:41 +0900

xdvik-ja (22.40v-j1.13-0.1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Tue, 11 Feb 2003 13:31:41 +0900

xdvik-ja (22.40v-j1.12-0.1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Tue,  4 Feb 2003 09:43:07 +0900

xdvik-ja (22.40u-j1.12-0.4) unstable; urgency=low

  * debian/xdvi-pl: Import from teTeX-bin-1.0.7+20021025 for /usr/bin/xdvi-ja.
  * debian/rules: Install the above script.
  * debian/postinst: Increase the alternative preinst to 35.
  * debian/preinst, debian/postrm, debian/postinst: Not divert /usr/bin/xdvi.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Thu, 30 Jan 2003 15:25:18 +0900

xdvik-ja (22.40u-j1.12-0.3) unstable; urgency=low

  * debian/rules, debian/preinst, debian/postrm: Divert /usr/bin/xdvi.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Tue, 28 Jan 2003 21:30:52 +0900

xdvik-ja (22.40u-j1.12-0.2) unstable; urgency=low

  * debian/postinst: Decrease the alternative priority to 20.
  * debian/rules, debian/patches/80xdvi-sh.diff: Revival /usr/bin/xdvi-ja.
  * These changes are installed, in order to obey Debian packaging policy.
    - debian/prerm: Do not remove vfontmap.
    - debian/postrm: Rename vfontmap to vfontmap.bak when this package is
      removed, and removed it when this package is purged.
    - debian/postinst: Use vfonmap.bak if found.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Tue, 28 Jan 2003 12:03:57 +0900

xdvik-ja (22.40u-j1.12-0.1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Thu, 23 Jan 2003 15:31:31 +0900

xdvik-ja (22.40t-j1.12-0.1) unstable; urgency=low

  * New upstream release.

 -- TSUCHIYA Masatoshi <tsuchiya@namazu.org>  Mon, 06 Jan 2003 10:29:08 +0900

xdvik-ja (22.40s-j1.12-0.3) unstable; urgency=low

  * Applied -/+vi patch from Masatoshi Tsuchiya.
  * Remove vfontmap only when prerm is called with "remove".
  * Now installs xdvik-ja specific files in /usr/share/texmf/xdvi-ja.
  * Now installed xdviprint.

 -- Masayuki Hatta <mhatta@debian.org>  Fri,  3 Jan 2003 01:08:05 +0900

xdvik-ja (22.40s-j1.12-0.2) unstable; urgency=low

  * Built with the normal libwww-dev.

 -- Masayuki Hatta <mhatta@debian.org>  Sun, 29 Dec 2002 22:21:40 +0900

xdvik-ja (22.40s-j1.12-0.1) unstable; urgency=low

  * NMU.
  * New upstream release (based on teTeX-src-beta-20021225) - closes: #76355
  * Now Build-Depends: libxaw7-dev - closes: #170011
  * Now generates /etc/texmf/vfontmap in postinst - closes: #138885
  * I guess this bug has gone, if not please reopen - closes: #94244

 -- Masayuki Hatta <mhatta@debian.org>  Sun, 29 Dec 2002 10:28:26 +0900

xdvik-ja (22.15-j1.04-4) unstable; urgency=low

  * installed new config.guess, config.sub. (closes: #106209)
  * made the build dependency on libxaw-dev (closes: #105002)
  * moved binaries to /usr/bin and man pages to /usr/share/man.
  * used update-alternatives for /usr/bin/xdvi.bin. (closes: #105767)

 -- Hayao Nakahara <nakahara@debian.org>  Fri, 17 Aug 2001 14:07:49 +0900

xdvik-ja (22.15-j1.04-3) unstable; urgency=low

  * added xlibs-dev to build dependency. (closes: #85333)
  * remove dh_suidregiste from debian/rules.
  * applied Tsuchiya's patch to debian/Makefile. (closes: #85140)
  * fixed debian/xdviprint program. (closes: #86354)

 -- Hayao Nakahara <nakahara@debian.org>  Tue, 20 Feb 2001 23:32:02 +0900

xdvik-ja (22.15-j1.04-2) unstable; urgency=low

  * use xdvi-ja.real in xdvi-ja script. (closes: #82078)
  * fixed xdviprint command. (added "rm -f $PSTMP")

 -- Hayao Nakahara <nakahara@debian.org>  Wed, 24 Jan 2001 21:23:02 +0900

xdvik-ja (22.15-j1.04-1) unstable; urgency=low

  * New upstream release.
  * Compiled with libkpathsea3. (closes: #80407)

 -- Hayao Nakahara <nakahara@debian.org>  Tue, 26 Dec 2000 01:16:08 +0900

xdvik-ja (22.15-j1.03-3) frozen unstable; urgency=low

  * fixed dangling symlink of document files. (closes: #56644)

 -- Hayao Nakahara <nakahara@debian.org>  Sun, 27 Feb 2000 01:03:22 +0900

xdvik-ja (22.15-j1.03-2) unstable; urgency=low

  * debian/control: Added Build-Depends filed.

 -- Hayao Nakahara <nakahara@debian.org>  Fri, 10 Dec 1999 15:31:55 +0900

xdvik-ja (22.15-j1.03-1) unstable; urgency=low

  * Initial Release for Debian.

 -- Hayao Nakahara <nakahara@debian.org>  Sun, 24 Oct 1999 11:00:03 +0900
